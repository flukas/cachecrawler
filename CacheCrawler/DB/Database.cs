﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.IO;

namespace CacheCrawler.DB
{
    class Database
    {
        private List<FileType> database;

        /// <summary>
        /// true if nothing has been added since last sorting
        /// </summary>
        protected bool sorted;

        /// <summary>
        /// 
        /// </summary>
        public Database()
        {
            database = new List<FileType>();
            sorted = false;
            //todo: auto load data?
        }

        /// <summary>
        /// Loads fileType data from a xml file
        /// </summary>
        /// <param name="fileName">path to file</param>
        public void LoadXml(string fileName)
        {
            sorted = false;
            XDocument d = XDocument.Load(fileName);
            IEnumerable<XElement> data = from p in d.Descendants("FileType") select p;
            foreach (XElement fileType in data)
            {
                database.Add(new FileType(fileType));
            }
        }

        public void SaveXml(string fileName)
        {
            XElement xe = new XElement("Extensions");
            foreach (FileType ft in database)
            {
                xe.Add(ft.Save());
            }
            XDocument d = new XDocument(xe);
            d.Save(fileName);

        }

        public void LoadTxt(string fileName)
        {

            StreamReader sr = new StreamReader(fileName);
            // for set encoding
            // StreamReader sr = new StreamReader(@"file.csv", Encoding.GetEncoding(1250));

            string strline = "";
            while (!sr.EndOfStream)
            {
                strline = sr.ReadLine();
                FileType ft = new FileType(strline);
                if (ft.Characters != null)
                {
                    database.Add(ft);
                }
            }
            sr.Close();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns>null if not in db, matching FileType otherwise</returns>
        public List<FileType> Match(byte[] bytes)
        {
            //Create pseudo FileType
            FileType toMatch = new FileType(bytes, "???");

            //search in db
            if (!sorted)
            {
                database.Sort();
            }

            //todo: binary search tree
            List<FileType> matching = new List<FileType>();
            foreach (FileType ft in database)
            {
                if (ft.Compare(ft, toMatch) == 0)
                {
                    //match found
                    matching.Add(ft);
                }
            }
            return matching;

        }

        public void CompareAll(byte[] bytes)
        {
                        //Create pseudo FileType
            FileType toMatch = new FileType(bytes, "???");

            //search in db
            if (!sorted)
            {
                database.Sort();
            }

            StreamWriter sw = new StreamWriter("AllCompare-" + DateTime.Now.ToString("yyMMddHHmmss") +".txt");
            sw.WriteLine("Comparing\t" + toMatch.HexCode +"\n");
            foreach (FileType ft in database)
            {
                int a = ft.CompareTo(toMatch);
                if (a==0)
                {
                    sw.WriteLine("***"+a.ToString() + "\t" + ft.Extension + "\t" + ft.HexCode + "\t" + ft.Description);
                }
                else
                {
                    sw.WriteLine(a.ToString() + "\t" + ft.Extension + "\t" + ft.HexCode + "\t" + ft.Description);
                }
                               
            }
            sw.Close();
        }

    }
}
