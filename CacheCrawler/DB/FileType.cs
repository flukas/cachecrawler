﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace CacheCrawler.DB
{
    public class FileType:IComparer<FileType>, IComparable<FileType>
    {
        public FileType(string strline)
        {
            if (strline[0] == '#')
            {
                Characters = null;
                Extension = null;
                Description = null;
            }
            else
            {
                string[] _values = strline.Split(',');
                Characters = HexToData(_values[0]);
                Extension = _values[1];
                Description = _values[2].Substring(1, _values[2].Length - 2);
            }
        }
        public FileType(XElement fileType)
        {
            Extension = fileType.Element("Extension").Value;
            Characters = HexToData(fileType.Element("Characters").Value);
            Description = fileType.Element("Description").Value;
        }
        public FileType(byte [] characters, string extension)
        {
            Characters = characters;
            Extension = extension;
        }

        public FileType(byte[] characters, string extension, string description)
        {
            Characters = characters;
            Extension = extension;
            Description = description;
        }

        public XElement Save()
        {
            XElement xe = new XElement("FileType",
                new XElement("Extension", Extension),
                new XElement("Characters", DataToHex(Characters), new XAttribute("type", "hex")),
                new XElement("Description", Description)
                );
            return xe;

        }
        public byte[] Characters;
        public string Extension;
        public string Description;


        #region IComparer<FileType> Members

        /// <summary>
        /// Compares two objects and returns a value indicating whether one is less than, equal to, or greater than the other.
        /// </summary>
        /// <param name="x">The first object to compare.</param>
        /// <param name="y">The second object to compare.</param>
        /// <returns>Value Condition 
        /// Less than zero - x is less than y.
        /// Zero x equals y.
        /// Greater than zero x is greater than y.
        /// </returns>
        public int Compare(FileType x, FileType y)
        {
            return CompareBytes(x.Characters, y.Characters);
            
        }
        
        
        #endregion

        #region IComparable<FileType> Members

        /// <summary>
        /// Compares the current object with another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>
        ///     A 32-bit signed integer that indicates the relative order of the objects
        ///     being compared. The return value has the following meanings: 
        ///     
        /// Value Meaning Less than zero This object is less than the other parameter.
        /// Zero This object is equal to other.    
        /// Greater than zero This object is greater than other.
        /// </returns>
        public int CompareTo(FileType other)
        {
            return CompareBytes(this.Characters, other.Characters);
        }

        /// <summary>
        /// Compares two byte arrays and returns a value indicating whether one is less than, equal to, or greater than the other.
        /// </summary>
        /// <param name="x">The first object to compare.</param>
        /// <param name="y">The second object to compare.</param>
        /// <returns>Value Condition 
        /// Less than zero - x is less than y.
        /// Zero x equals y.
        /// Greater than zero x is greater than y.
        /// </returns>
        private int CompareBytes(byte[] x, byte[] y)
        {
            int i = 0;
            //bool end = false;
            //todo: rozdílá délka => shoda?
            while (x[i] == y[i])
            {
                i++;
                if (x.Length < i + 2 || y.Length < i + 2)
                {
                    return 0;
                }
            }
            if (x[i] < y[i])
            {
                return -1;
            }
            else
            {
                return 1;
            }
 
        }

        #endregion

        public override string ToString()
        {

            return Extension +", "+ DataToHex(Characters);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hexString"></param>
        /// <returns></returns>
        protected byte[] HexToData(string hexString)
        {
            if (hexString == null)
                return null;

            if (hexString.Length % 2 == 1)
                hexString = hexString + '0'; // Up to you whether to pad the first or last byte

            byte[] data = new byte[hexString.Length / 2];

            for (int i = 0; i < data.Length; i++)
                data[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);

            return data;
        }

        protected string DataToHex(byte[] data)
        {
            //todo: use stringbuilder or other more efficient way
            string byteString = "";
            for (int i = 0; i < data.Length; i++)
            {
                byteString += data[i].ToString("X2");
            }
            return byteString;
        }

        public string HexCode
        {
            get
            {
                return DataToHex(Characters);
            }
        }
    }
}
