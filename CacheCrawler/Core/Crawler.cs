﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using CacheCrawler.DB;

namespace CacheCrawler.Core
{
    public class ProgressChangeEventArgs : EventArgs
    {
        public ProgressChangeEventArgs(int currentItem, int totalItems)
        {
            TotalItems = totalItems;
            CurrentItem = currentItem;
        }
        public int TotalItems;
        public int CurrentItem;
    }

    class Crawler
    {
        public event EventHandler<ProgressChangeEventArgs> ProgressChange; 

        protected const int firstBytes = 24;
        protected string folderName;
        protected String output;
        protected Database db;

        public Crawler()
        {

        }
        public Crawler(string folderName, string output, Database db)
        {
            StreamWriter sw = new StreamWriter(output);
            sw.WriteLine("Processing " + folderName);

            DirectoryInfo di = new DirectoryInfo(folderName);
            FileInfo[] fia = di.GetFiles();
            foreach (FileInfo fi in fia)
            {
                sw.Write(fi.Extension);


                sw.Write(" might be ");
                List<FileType> ftl = db.Match(GetBytes(fi.FullName, 0, firstBytes));
                string se = string.Empty;
                foreach (FileType ft in ftl)
                {
                    se += ft.Extension + ", ";
                }
                sw.Write(se);
                sw.Write("---");
                sw.WriteLine(fi.FullName);
            }
            sw.Close();
        }

        public Crawler(string folderName, string output, Database db, bool harvest)
        {
            this.folderName = folderName;
            this.output = output;
            this.db = db;
        }
        public void Harvest()
        {
            try
            {


                StreamWriter sw = new StreamWriter(output);
                sw.WriteLine("Processing " + folderName);

                Directory.CreateDirectory(folderName + "\\harvest");
                DirectoryInfo di = null;
                bool directoryLoaded = false;
                while (!directoryLoaded)
                {
                    try
                    {
                        di = new DirectoryInfo(folderName);
                        directoryLoaded = true;
                    }
                    catch (Exception e)
                    {

                        System.Windows.Forms.MessageBox.Show("Exception at loading directory");
                    }
                }
                FileInfo[] fia = di.GetFiles();
                int progressMeter = 0;
                foreach (FileInfo fi in fia)
                {
                    progressMeter++;
                    sw.Write(fi.Extension);


                    sw.Write(" copied to (first) ");

                    List<FileType> ftl = null; ;
                    //bool fileMatched = false;
                    //while (!fileMatched)
                    //{
                    try
                    {
                        ftl = db.Match(GetBytes(fi.FullName, 0, firstBytes));
                        //fileMatched = true;
                        string se = string.Empty;
                        foreach (FileType ft in ftl)
                        {
                            se += ft.Extension + ", ";
                        }
                        if (ftl.Count > 0)
                        {
                            try
                            {
                                File.Copy(fi.FullName, fi.DirectoryName + "\\harvest\\" + fi.Name + "." + ftl[0].Extension);
                                EventHandler<ProgressChangeEventArgs> temp = ProgressChange;
                                if (temp != null)
                                {
                                    temp(this,new ProgressChangeEventArgs(progressMeter,fia.Length));
                                }
                            }
                            catch (Exception e)
                            {

                                System.Windows.Forms.MessageBox.Show("Exception at copying file");
                            }
                        }
                    sw.Write(se);
                    sw.Write("---");
                    sw.WriteLine(fi.FullName);
                    }
                    catch (Exception e)
                    {

                        System.Windows.Forms.MessageBox.Show("Exception at Matching file");
                    }
                    //}


                }
                sw.Close();
            }
            catch (Exception e)
            {

                System.Windows.Forms.MessageBox.Show("Other exception");
            }

        }

        public byte[] GetBytes(string fileName, uint start, uint end)
        {
            //get first bytes
            byte[] buff = null;
            FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);
            long numBytes = new FileInfo(fileName).Length;
            br.ReadBytes((int)start);
            //todo: obecně s offsetem a počítáním od konce
            buff = br.ReadBytes((int)Math.Min(numBytes - start, end));
            return buff;
        }

        //protected string GetType (string fileName)
        //{
        //    byte[] buff = GetBytes(fileName, 0, firstBytes);
        //    return null;


        //}

    }
}
