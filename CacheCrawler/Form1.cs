﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CacheCrawler.DB;
using CacheCrawler.Core; 

namespace CacheCrawler
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        Database db;
        private void button1_Click(object sender, EventArgs e)
        {
            db = new Database();
            db.LoadXml("Extensions.xml");

        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult dr = openFileDialog1.ShowDialog();
            if (dr == DialogResult.OK )
            {
                Core.Crawler crw = new CacheCrawler.Core.Crawler();
                byte[] ba = crw.GetBytes(openFileDialog1.FileName, 0, 20);
                List<FileType> ftl = db.Match(ba);
                if (ftl.Count!= 0)
                {
                    string se = string.Empty;
                    string sc = string.Empty;
                    foreach (FileType ft in ftl)
                    {
                        se+=ft.Extension + ", ";
                        sc += ft.HexCode + "\n";
                    }
                    textBox1.Text=se;
                    MessageBox.Show((new FileType(ba,"?")).HexCode +"\n\n\n"+sc);
                }
                else
                {
                    textBox1.Text = "Not found in DB";
                }
                //textBox1.Text=ft.Extension;
            }
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            db = new Database();
            db.LoadTxt("headersig.txt");
            MessageBox.Show("Done");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            db.SaveXml("headersig.xml");
            MessageBox.Show("Done");
        }

        private void button3_Click(object sender, EventArgs e)

        {
            DialogResult dr = openFileDialog1.ShowDialog();
            if (dr == DialogResult.OK)
            {
                Core.Crawler crw = new CacheCrawler.Core.Crawler();
                db.CompareAll(crw.GetBytes(openFileDialog1.FileName, 0, 25));
                MessageBox.Show("Done");
            }

        }

        private void button6_Click(object sender, EventArgs e)
        {
            DialogResult dr = folderBrowserDialog1.ShowDialog();
            if (dr == DialogResult.OK)
            {
                //TODO:hodí výjimku, pokud neexistuje c:\temp\x
                Core.Crawler crw = new CacheCrawler.Core.Crawler(folderBrowserDialog1.SelectedPath, "c:\\temp\\x\\Crawl-" + DateTime.Now.ToString("yyMMddHHmmss") + ".txt", db);
                MessageBox.Show("Done");

            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            
            DialogResult dr = folderBrowserDialog1.ShowDialog();
            if (dr == DialogResult.OK)
            {
                Core.Crawler crw = new CacheCrawler.Core.Crawler(folderBrowserDialog1.SelectedPath, "c:\\temp\\x\\Crawl-" + DateTime.Now.ToString("yyMMddHHmmss") + ".txt", db,true);
                crw.ProgressChange += new EventHandler<ProgressChangeEventArgs>(crw_ProgressChange);
                crw.Harvest();
                MessageBox.Show("Done");

            }
        }

        void crw_ProgressChange(object sender, ProgressChangeEventArgs e)
        {
            progressBar1.Value = (int)Math.Round((double)e.CurrentItem / (double)e.TotalItems * 100d);
        }
    }
}
